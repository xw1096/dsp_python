import pyaudio
import wave
import struct
import math
import sys

from myfunctions import clip16

wavfile = sys.argv[1]

wf = wave.open( wavfile, 'rb' )

# Read the wave file properties
num_channels = wf.getnchannels()       	# Number of channels
RATE = wf.getframerate()                # Sampling rate (frames/second)
signal_length  = wf.getnframes()       	# Signal length
width = wf.getsampwidth()       		# Number of bytes per sample

stream = p.open(format      = pyaudio.paInt16,
                channels    = num_channels,
                rate        = RATE,
                input       = False,
                output      = True )

# Get first frame
input_string = wf.readframes(1)

while input_string != '':
    
    if num_channels == 1:
    # Convert string to number
        input_tuple = struct.unpack('h', input_string)  # One-element tuple
        input_value = input_tuple[0]                    # Number

    # Set input to difference equation
        x0 = input_value


    # Convert output value to binary string
        output_string = struct.pack('h', x0)  
    else:
        input_tuple = struct.unpack('hh', input_string)
        x0 = input_tuple[0]
        x1 = input_tuple[1]
        out_string = struct.pack('h', x0)
        out_string +=struct.pack('h', x1)
    # Write binary string to audio stream
    stream.write(output_string)                     

    # Get next frame
    input_string = wf.readframes(1)

print("**** Done ****")
