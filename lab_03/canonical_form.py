import pyaudio
import wave
import struct
import math
import matplotlib.pyplot as plt
from myfunctions import clip16

wavfile = 'author.wav'

wf = wave.open(wavfile, 'rb')
wf_can = wave.open('canonical_form.wav', 'w')
wf_dir = wave.open('direct_form.wav', 'w')

num_channels = wf.getnchannels()       	# Number of channels
RATE = wf.getframerate()                # Sampling rate (frames/second)
signal_length  = wf.getnframes()       	# Signal length
width = wf.getsampwidth()       		# Number of bytes per sample

print("The file has %d channel(s)."            % num_channels)
print("The frame rate is %d frames/second."    % RATE)
print("The file has %d frames."                % signal_length)
print("There are %d bytes per sample."         % width)

wf_can.setnchannels(num_channels)
wf_can.setsampwidth(width)
wf_can.setframerate(RATE)
wf_dir.setnchannels(num_channels)
wf_dir.setsampwidth(width)
wf_dir.setframerate(RATE)

# Difference equation coefficients
b0 =  0.008442692929081
b1 = 0.0
b2 = -0.016885385858161
b3 = 0.0
b4 =  0.008442692929081

# a0 =  1.000000000000000
a1 = -3.580673542760982
a2 =  4.942669993770672
a3 = -3.114402101627517
a4 =  0.757546944478829

# Initialization
w1 = 0
w2 = 0
w3 = 0
w4 = 0
x1 = 0.0
x2 = 0.0
x3 = 0.0
x4 = 0.0
y1 = 0.0
y2 = 0.0
y3 = 0.0
y4 = 0.0

p = pyaudio.PyAudio()

stream = p.open(format      = pyaudio.paInt16,
                channels    = num_channels,
                rate        = RATE,
                input       = False,
                output      = True )

input_string = wf.readframes(1)

print("play the wave file %s in canonical form" % wavfile)
while input_string != '':

    # Convert string to number
    input_tuple = struct.unpack('h', input_string)  # One-element tuple
    input_value = input_tuple[0]                    # Number

    # Set input to difference equation
    x0 = input_value

    # Difference equation
    w0 = x0 - a1*w1 - a2*w2 - a3*w3 - a4*w4
    y0 = b0*w0 + b1*w1 + b2*w2 + b3*w3 + b4*w4

    # Delays
    w4 = w3
    w3 = w2
    w2 = w1
    w1 = w0

    # Compute output value
    output_value = clip16(y0)    # Number

    # Convert output value to binary string
    output_string = struct.pack('h', output_value)  

    # Write binary string to audio stream
    stream.write(output_string)
    wf_can.writeframes(output_string)                     

    # Get next frame
    input_string = wf.readframes(1)
wf.close()

print("play the wave file %s in direct form" % wavfile)
wf = wave.open(wavfile, 'rb')
input_string = wf.readframes(1)
while input_string != '':

    # Convert string to number
    input_tuple = struct.unpack('h', input_string)  # One-element tuple
    input_value = input_tuple[0]                    # Number

    # Set input to difference equation
    x0 = input_value

    # Difference equation
    y0 = b0*x0 + b2*x2 + b4*x4 - a1*y1 - a2*y2 - a3*y3 - a4*y4 

    # Delays
    x4 = x3
    x3 = x2
    x2 = x1
    x1 = x0
    y4 = y3
    y3 = y2
    y2 = y1
    y1 = y0

    # Compute output value
    output_value = clip16(y0)    # Number

    # Convert output value to binary string
    output_string = struct.pack('h', output_value)

    # Write binary string to audio stream
    stream.write(output_string)                     
    wf_dir.writeframes(output_string)
    # Get next frame
    input_string = wf.readframes(1)

print("**** Done ****")  

stream.stop_stream()
stream.close()
p.terminate()
wf.close

# Verify
wf_can = wave.open('canonical_form.wav', 'rb')
wf_dir = wave.open('direct_form.wav', 'rb')
can_list = []
dir_list = []
can_dir = []

can_input = wf_can.readframes(1)
dir_input = wf_dir.readframes(1)
while can_input != '' and dir_input != '':
    can_value = struct.unpack('h', can_input)[0]
    dir_value = struct.unpack('h', dir_input)[0]
    can_dir_value = can_value - dir_value
    can_list.append(can_value)
    dir_list.append(dir_value)
    can_dir.append(can_dir_value)
    can_input = wf_can.readframes(1)
    dir_input = wf_dir.readframes(1)

fig = plt.figure()
ax1 = fig.add_subplot(311)
ax1.plot(can_list)
ax1.set_title('canonical form wave')
ax2 = fig.add_subplot(312)
ax2.plot(dir_list)
ax2.set_title('direct form wave')
ax3 = fig.add_subplot(313)
ax3.plot(can_dir)
ax3.set_title('canonical form substract direct form')
plt.show()
