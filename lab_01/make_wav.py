from struct import pack
from math import sin, pi
import wave

fs = 8000

wf = wave.open('sin.wav', 'w')
wf.setnchannels(1)
wf.setsampwidth(1)
wf.setframerate(fs)
maxAmp = 2**7-1.0
f = 261.625565

for n in range(0, 1*fs):
    wvData = pack('B', maxAmp+maxAmp*sin(n*2*pi*f/fs))
    wf.writeframes(wvData)
wf.close()
