import wave
from struct import pack
from math import sin, pi

Fs = 8000
wf = wave.open('sinstereo.wav', 'w')
wf.setnchannels(4)			# two channels (stereo)
wf.setsampwidth(2)			# two bytes per sample
wf.setframerate(Fs)			# samples per second
maxAmp = 2**15-1.0 			# maximum amplitude
f1 = 261.625565  			# 261.625565 Hz (middle C)
f2 = 440.0  				# note A4
f3 = 500
f4 = 1000
for n in range(0, 1*Fs):	# 1 second duration
    wvData = pack('h', maxAmp * sin(n*2*pi*f1/Fs)) # left
    wvData += pack('h', maxAmp * sin(n*2*pi*f2/Fs)) #left middle
    wvData += pack('h', maxAmp * sin(n*2*pi*f3/Fs)) #right middle
    wvData += pack('h', maxAmp * sin(n*2*pi*f4/Fs)) # right
    # 'h' indicates 'short'
    wf.writeframes(wvData)
wf.close()
