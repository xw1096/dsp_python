import pyaudio
import struct
import scipy as sp
from scipy.fftpack import fft, ifft
from myfunctions import stft, istft

RATE = 16000

win_size = 512
window_1 = sp.array([sp.cos(sp.pi*(i+0.5)/win_size - sp.pi/2) for i in xrange(win_size/2)])
window_2 = sp.array([sp.cos(sp.pi*(i+256.5)/win_size - sp.pi/2) for i in xrange(win_size/2)])

p = pyaudio.PyAudio()
stream = p.open(format = p.get_format_from_width(2),
                channels = 1,
                rate = 16000,
                input  =True,
                output = True,
                frames_per_buffer = 2048)

while True:
    
    input_string = stream.read(win_size/2)
    
    input_value = sp.array(struct.unpack('h'*(win_size/2), input_string))

    tmp_1 = sp.fft(window_1*input_value)
    tmp_2 = sp.fft(window_2*input_value)
    tmp_1 = sp.absolute(tmp_1)
    tmp_2 = sp.absolute(tmp_2)
    tmp = window_1*sp.ifft(tmp_1) + window_2*sp.ifft(tmp_2)
    tmp = 10000*tmp/100000.0
    
    output_string = struct.pack('h'*(win_size/2), *tmp)
    
    stream.write(output_string)     
    
