

% Load signal
% [x, fs] = wavread('author.wav');
[x, fs] = audioread('sp1.wav');
N = length(x);

%% Compute STFT
R = 512 ;
Nfft = 512;
X = stft(x, R, Nfft);
% X = fft(x);
% Set phase to zero in STFT-domain
X2 = abs(X);

% Synthesize new signal ('robotic')
y2 = inv_stft(X2, R, N);
% y2 = ifft(X2);
%%

y2 = y2/max(abs(y2));

audiowrite('sp1_test.wav', y2, fs);

soundsc(y2, fs);

