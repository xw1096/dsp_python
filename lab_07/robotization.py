import pyaudio
import wave
import struct
import scipy as sp
from myfunctions import stft, istft

wavefile = '1_3.wav'

print 'robotization the wave file: {0:s}.' .format(wavefile)

wf = wave.open(wavefile, 'rb')
RATE = wf.getframerate()

win_size = 512
overlap = win_size/2
# window = sp.array([sp.cos(sp.pi*(i+0.5)/win_size - sp.pi/2) for i in xrange(win_size)])
window_1 = sp.array([sp.cos(sp.pi*(i+0.5)/win_size - sp.pi/2) for i in xrange(overlap)])
window_2 = sp.array([sp.cos(sp.pi*(i+256.5)/win_size - sp.pi/2) for i in xrange(overlap)])

p = pyaudio.PyAudio()
stream = p.open(format = p.get_format_from_width(wf.getsampwidth()),
                channels = wf.getnchannels(),
                rate = RATE,
                output = True,
                frames_per_buffer = win_size)

# initial data

# input_string = wf.readframes(win_size)
# input_value = sp.array(struct.unpack('h'*(win_size), input_string))

# tmp = sp.append(zeros(overlap), input_value[0:overlap])
# tmp = sp.absolute(sp.fft(window*tmp))

input_string = wf.readframes(overlap)
print len(input_string)
while len(input_string) == win_size:
   
    input_value = sp.array(struct.unpack('h'*(overlap), input_string))

    tmp_1 = sp.fft(window_1*input_value)
    tmp_2 = sp.fft(window_2*input_value)
    tmp_1 = sp.absolute(tmp_1)
    tmp_2 = sp.absolute(tmp_2)
    tmp = window_1*sp.ifft(tmp_1) + window_2*sp.ifft(tmp_2)
    tmp = 100000*tmp/100000.0
    # print tmp    

    output_string = struct.pack('h'*(overlap), *tmp)
    
    stream.write(output_string)    

    input_string = wf.readframes(overlap)

print '* Done'

stream.stop_stream()
stream.close()
p.terminate()  
