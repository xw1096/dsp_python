
%% Yes/No data

load yes   % loads 'yes' data
load no    % loads 'no' data

% (Each row is a single signal)

Fs = 8000;      % Sampling rate (samples/second)

N = size(yes, 2);   % Length of signals

Nfft = 2^12;    % 2^12 = 4096.   (Make sure that Nfft >= N)

t = (0:N-1) / Fs;
f = (0:Nfft-1) / Nfft * Fs;

dB = @(x) 20*log10(abs(x));

%%

K = 12;   % Number of 'yes' and 'no'

figure(1)
clf
for k = 1:K
    
    x = no(k, :);
    X = fft(x, Nfft);
    
    subplot(2, 1, 1)
    plot(t, x)
    xlabel('Time (sec)')
    title(sprintf('''No'', number %d', k))
    
    subplot(2, 1, 2)
    plot(f, dB(X))
    xlim([0 Fs/2])
    title('Spectrum')
    xlabel('Frequency (Hz)')
    
    drawnow
    soundsc(x, Fs)
    
end

%%

figure(1)
clf
for k = 1:K
    x = yes(k, :);
    X = fft(x, Nfft);
    
    subplot(2, 1, 1)
    plot(t, x)
    xlabel('Time (sec)')
    title(sprintf('''Yes'', number %d', k))
    
    subplot(2, 1, 2)
    plot(f, dB(X))
    xlim([0 Fs/2])
    title('Spectrum')
    xlabel('Frequency (Hz)')
    
    drawnow
    soundsc(x, Fs)

end

