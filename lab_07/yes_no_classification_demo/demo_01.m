%% Yes/No classification
% Use two filters

close all, clear all

%% Load data

load yes   % loads 'yes' data
load no    % loads 'no' data

% (Each row is a single signal)

Fs = 8000;      % Sampling rate (samples/second)

N = length(yes(1, :));
t = (1:N)/Fs;

%% Example 'yes' and 'no'

figure(1)
clf

subplot(2, 1, 1)
plot(t, no(5, :) )
title('no')
xlabel('Time (sec)')
zoom xon

subplot(2, 1, 2)
plot(t, yes(4, :) )
title('yes')
xlabel('Time (sec)')
zoom xon

%%

soundsc( no(5, :), Fs)
soundsc( yes(4, :), Fs)


%% Make 2 filters

[b1, a1] = cheby2(5, 50, [ 500 1500]/(Fs/2) );  % Band-pass filter (500 - 1500 Hz)
[b2, a2] = cheby2(5, 50, [1500 3500]/(Fs/2) );  % Band-pass filter (1500 - 3500 Hz)

[H1, om] = freqz(b1, a1);
[H2, om] = freqz(b2, a2);
f = om/pi*Fs/2;

figure(2)
clf
plot(f, abs(H1), f, abs(H2) )
xlabel('Frequency (Hz)')
title('Frequency responses of two filters')

zoom yon

print -dpdf demo_01_filters


%% Look at data

K = 13;  % Number of 'yes', 'no'

M = 800  % 100 msec at Fs = 8000 samples/sec
m = 1:M;

Nwords = 2;   % Classify two words ('yes', 'no')

p = zeros(Nwords, K, 2);   % Array of features

for i = 1:Nwords
    
    for k = 1:K
        
        switch i
            case 1
                word = 'yes'
                s = yes(k, :);
            case 2
                word = 'no'
                s = no(k, :);
        end
        
        % Find maximum value of signal
        [~, q] = max(abs(s));
        
        % Apply filters
        s1 = filter(b1, a1, s);         % filter 1
        s2 = filter(b2, a2, s);         % filter 2
        
        % Extract segment of length M centered around maximum
        q = q - M/2;
        if q < 1, q = 1; end;
        x  = s(q + m);
        x1 = s1(q + m);
        x2 = s2(q + m);
        
        % Calculate average signal amplitdue over segment
        c1 = mean(abs(x1));
        c2 = mean(abs(x2));

        % Normalize (removes variation due to amplitude scaling)
        v1 = c1/(c1 + c2);
        v2 = c2/(c1 + c2);

        % Save features in data array        
        p(i, k, 1) = v1;
        p(i, k, 2) = v2;
       
        figure(1)
        clf
        
        subplot(2, 2, 1)
        plot(t, s)
        title(['Speech: ', word, '. Number ', num2str(k)])
        xlabel('Time (samples)')
        axis tight
        
        subplot(2, 2, 2)
        plot(t(q+m), x)
        title('Speech segment')
        xlabel('Time (sec)')
        axis tight
        ax = axis;

        subplot(2, 2, 3)
        plot(t(q+m), x1)
        title('Speech segment - filter 1')
        xlabel('Time (sec)')
        axis(ax)
        
        subplot(2, 2, 4)
        plot(t(q+m), x2)
        title('Speech segment - filter 2')
        xlabel('Time (sec)')
        axis(ax)

        drawnow
        
    end
    
end


%%


figure(2)
clf

plot(p(1, 1:12, 1), p(1, 1:12, 2) , 'r.')
hold on
plot(p(2, 1:12, 1), p(2, 1:12, 2) , 'bo')

legend('YES', 'NO')


%%

figure(2)
clf

plot(p(1,1:12,1), p(1,1:12,2) , 'x', p(2,1:12,1), p(2,1:12,2) , 'o')
hold on
plot(p(1,13,1),p(1,13,2),'gx',p(2,13,1),p(2,13,2),'yo')
legend('YES', 'NO','MY\_YES','MY\_NO')
xlabel('Feature 1 (500 - 1500 Hz)')
ylabel('Feature 2 (1500 - 3500 Hz)')

line([0 1], [0 .3])

axis square

print -dpdf demo_01_data




