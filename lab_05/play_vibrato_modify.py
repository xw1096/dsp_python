# play_vibrato_ver2.py
# Reads a specified wave file (mono) and plays it with a vibrato effect.
# (Sinusoidal time-varying delay)
# This implementation uses a circular buffer with two buffer indices.
# Uses linear interpolation

import pyaudio
import wave
import struct
import math
import pygame
from myfunctions import clip16
pygame.init()
pygame.display.set_mode((100, 100))
# TRY BOTH WAVE FILES
wavfile = 'author.wav'
# wavfile = 'decay_cosine_mono.wav'
# wavfile = 'sin01_mono.wav'
print 'Play the wave file: {0:s}.'.format(wavfile)

# Open wave file
wf = wave.open( wavfile, 'rb')

# Read wave file properties
CHANNELS = wf.getnchannels()        # Number of channels
RATE = wf.getframerate()            # Sampling rate (frames/second)
LEN  = wf.getnframes()              # Signal length
WIDTH = wf.getsampwidth()           # Number of bytes per sample

print('The file has %d channel(s).'         % CHANNELS)
print('The file has %d frames/second.'      % RATE)
print('The file has %d frames.'             % LEN)
print('The file has %d bytes per sample.'   % WIDTH)

# Vibrato parameters
f0 = 2
W = 0.2
# W = 0 # for no effct

f1 = 10

# OR
# f0 = 20
# ratio = 1.06
# W = (ratio - 1.0) / (2 * math.pi * f0 )
# print W

# Create a buffer (delay line) for past values
buffer_MAX =  1024                          # Buffer length
buffer = [0.0 for i in range(buffer_MAX)]   # Initialize to zero

# Initialize blocking
block_size = 128

# Buffer (delay line) indices
kr_0 = 0
kr_1 = 0  # read index
kw = int(0.5 * buffer_MAX)  # write index (initialize to middle of buffer)
kw = buffer_MAX/2

# print('The delay of {0:.3f} seconds is {1:d} samples.'.format(delay_sec, delay_samples))
print 'The buffer is {0:d} samples long.'.format(buffer_MAX)

# Open an output audio stream
p = pyaudio.PyAudio()
stream = p.open(format      = pyaudio.paInt16,
                channels    = 2,
                rate        = RATE,
                input       = False,
                output      = True )
stream_in = p.open(format      = pyaudio.paInt16,
                   channels    = 1,
                   rate        = RATE,
                   input       = True,
                   output      = False )

output_all = ''            # output signal in all (string)

print ('* Playing...')

# Loop through wave file 
for n in range(0, LEN):

    # Get sample from wave file
    input_string = stream_in.read(block_size)
    # Convert string to number
    input_value = struct.unpack('h'*block_size, input_string)
    
    
    output_string = ''
    for i in xrange(block_size):
    # Get previous and next buffer values (since kr is fractional)
        kr_prev_0 = int(math.floor(kr_0))               
        kr_next_0 = kr_prev_0 + 1
        frac_0 = kr_0 - kr_prev_0    # 0 <= frac < 1
        if kr_next_0 >= buffer_MAX:
            kr_next_0 = kr_next_0 - buffer_MAX
        kr_prev_1 = int(math.floor(kr_1))               
        kr_next_1 = kr_prev_1 + 1
        frac_1 = kr_1 - kr_prev_1    # 0 <= frac < 1
        if kr_next_1 >= buffer_MAX:
            kr_next_1 = kr_next_1 - buffer_MAX
    # Compute output value using interpolation
        output_value_0 = (1-frac_0) * buffer[kr_prev_0] + frac_0 * buffer[kr_next_0]
        output_value_1 = (1-frac_1) * buffer[kr_prev_1] + frac_1 * buffer[kr_next_1]
    # Update buffer (pure delay)
        buffer[kw] = input_value[i]

    # Increment read index
        kr_0 = kr_0 + 1 + W * math.sin( 2 * math.pi * f0 * (n*block_size+i) / RATE )
        kr_1 = kr_1 + 1 + W * math.sin( 2 * math.pi * f1 * (n*block_size+i) / RATE )
        # Note: kr is fractional (not integer!)

    # Ensure that 0 <= kr < buffer_MAX
        if kr_0 >= buffer_MAX:
        # End of buffer. Circle back to front.
            kr_0 = 0
        if kr_1 >= buffer_MAX:
            kr_1 = 0

    # Increment write index    
        kw = kw + 1
        if kw == buffer_MAX:
        # End of buffer. Circle back to front.
            kw = 0

    # Clip and convert output value to binary string
        output_string += struct.pack('h', clip16(output_value_0))
        output_string += struct.pack('h', clip16(output_value_1))

    # Write output to audio stream
    stream.write(output_string)
    output_all = output_all + output_string     # append new to total

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                pygame.quit()
                #sys.exit()
            if event.key == pygame.K_w:
                print "press w"


print('* Done')

stream.stop_stream()
stream.close()
p.terminate()

output_wavefile = wavfile[:-4] + '_vibrato.wav'
print 'Writing to wave file', output_wavefile
wf = wave.open(output_wavefile, 'w')      # wave file
wf.setnchannels(1)      # one channel (mono)
wf.setsampwidth(2)      # two bytes per sample
wf.setframerate(RATE)   # samples per second
wf.writeframes(output_all)
wf.close()
print('* Done')

