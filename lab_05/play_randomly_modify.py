# play_randomly.py
"""
PyAudio Example: Generate random pulses and input them to an IIR filter of 2nd order.
It sounds like pings from a Sonar or from a Geiger Counter.
Gerald Schuller, March 2015 
Modified - Ivan Selesnick, October 2015
"""

import pyaudio
import struct
from math import sin, cos, pi
import random
from myfunctions import clip16
from matplotlib import pyplot as plt
# import numpy as np

BLOCKSIZE = 1024    # Number of frames per block
WIDTH = 2           # Bytes per sample
CHANNELS = 2
RATE = 8000         # Sampling rate in Hz

# Parameters
T = 10      # Total play time (seconds)
Ta = 0.4    # Decay time (seconds)
f1 = 350    # Frequency (Hz)
f2 = 500
# Pole radius and angle
r = 0.01**(1.0/(Ta*RATE))       # 0.01 for 1 percent amplitude
om1 = 2.0 * pi * float(f1)/RATE
r_2 = 0.02**(1.0/(Ta*RATE))
om2 = 2.0*pi*float(f2)/RATE
# Filter coefficients (second-order IIR)
a1 = -2*r*cos(om1)
a2 = r**2
b0 = sin(om1)
a3 = -2*r_2*cos(om2)
a4 = r_2**2
b1 = sin(om2)

NumBlocks = T * RATE / BLOCKSIZE

y = [0 for i in range(BLOCKSIZE)]
y_2 = [0 for i in xrange(BLOCKSIZE)]

plt.ion()           # Turn on interactive mode so plot gets updated
fig = plt.figure(1)
line_1, = plt.plot([j-3200 for j in y])
line_2, = plt.plot([i+32000 for i in y_2])
plt.ylim(-64000, 64000)
plt.xlim(0, BLOCKSIZE)
plt.xlabel('Time (n)')
plt.legend([line_1, line_2], ['Left channel', 'Right channel'])
plt.show()

# Open the audio output stream
p = pyaudio.PyAudio()
PA_FORMAT = p.get_format_from_width(WIDTH)
stream = p.open(format = PA_FORMAT,
                channels = CHANNELS,
                rate = RATE,
                input = False,
                output = True)

print 'Playing for {0:f} seconds ...'.format(T),

THRESHOLD = 2.5 / RATE          # For a rate of 2.5 impulses per second
THRESHOLD_2 = 5.0/ RATE
# Loop through blocks
for k in range(0, NumBlocks):

    # Do difference equation for block
    data = ''
    for n in range(BLOCKSIZE):

        rand_val = random.random()
        
        if rand_val < THRESHOLD:
            x = 15000
        else:
            x = 0
        if rand_val < THRESHOLD_2:
            x_2 = 15000
        else:
            x_2 = 0
        y[n] = b0 * x - a1 * y[n-1] - a2 * y[n-2]  
              # What happens when n = 0?
              # In Python negative indices cycle to end, so it works..
        y_2[n] = b1*x_2 - a3*y_2[n-1] - a4*y_2[n-2]
        y[n] = clip16(y[n])
        
        y_2[n] = clip16(y_2[n])
    # If numpy is available, then clipping can be done using:
    # y = np.clip(y, -32000, 32000)
        data += struct.pack('h', y_2[n])
        data += struct.pack('h', y[n])
    # Convert numeric list to binary string
    line_1.set_ydata([j-32000 for j in y])
    line_2.set_ydata([i+32000 for i in y_2])
    plt.title('Block {0:d}'.format(k))
    plt.pause(0.0001)    
    # Write binary string to audio output stream
    stream.write(data)

print 'Done.'

stream.stop_stream()
stream.close()
p.terminate()
