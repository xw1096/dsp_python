import pyaudio
import struct
import numpy as np
from math import sin, cos, pi
from myfunctions import clip16
from matplotlib import pyplot as plt

blocksize = 1024
width = 2
channels = 1
rate = 16000

initial_freq = 200
final_freq = 500
t = 5
t_samples = t * rate
curve = float(initial_freq - final_freq)/t_samples

numblock = t_samples/blocksize
maxAmp = 2**15-1.0

y = [0 for i in xrange(blocksize)]

plt.ion()
fig = plt.figure(1)
plt.subplot(2, 1, 1)
plt.title('time domain')
line_1, = plt.plot(y)
plt.subplot(2, 1, 2)
plt.title('frequency domain')
line_2, = plt.plot(y)
plt.show()

p = pyaudio.PyAudio()
PA_FORMAT = p.get_format_from_width(width)
stream = p.open(format = PA_FORMAT,
                channels = channels,
                rate = rate,
                input = False,
                output = True)

for k in xrange(0, numblock):
    
    for n in xrange(blocksize):
        f = curve*(blocksize*k+n)
        y[n] = maxAmp * sin((blocksize*k+n)*2*pi*f/rate)
    
    
    Y = np.fft.fft(y)/len(y)
    line_1.set_ydata(y)
    plt.subplot(2, 1, 1)
    plt.ylim(-max(y),max(y))
    line_2.set_ydata(abs(Y))
    plt.subplot(2, 1, 2)
    plt.ylim(0, max(abs(Y)))
    plt.pause(0.0001)
    data = struct.pack('h'*blocksize, *y)
    stream.write(data)

print 'done'

stream.stop_stream()
stream.close()
p.terminate()
