# playAM_blocking_fix.py
# Play a mono wave file with amplitude modulation. 
# This implementation reads and plays a block at a time (blocking)
# and corrects for block-to-block angle mismatch.
# Assignment: modify this file so it works for both mono and stereo wave files
#  (where does this file have an error when wave file is stereo and why? )

# f0 = 0      # Normal audio
f0 = 1600    # 'Duck' audio

BLOCKSIZE = 2048      # Number of frames per block

import pyaudio
import struct
import wave
import math
from matplotlib import pyplot as plt
import numpy as np

# Open wave file (mono)
wave_file_name = 'author.wav'
# wave_file_name = 'sin01_mono.wav'
# wave_file_name = 'sin01_stereo.wav'
wf = wave.open( wave_file_name, 'rb')
RATE = wf.getframerate()
WIDTH = wf.getsampwidth()
LEN = wf.getnframes() 
CHANNELS = wf.getnchannels() 

print 'The sampling rate is {0:d} samples per second'.format(RATE)
print 'Each sample is {0:d} bytes'.format(WIDTH)
print 'The signal is {0:d} samples long'.format(LEN)
print 'The signal has {0:d} channel(s)'.format(CHANNELS)

# Open audio stream
p = pyaudio.PyAudio()
stream = p.open(format = p.get_format_from_width(WIDTH),
                channels = 1,
                rate = RATE,
                input = False,
                output = True)

# Create block (initialize to zero)
output_block = [0 for n in range(0, BLOCKSIZE)]

plt.ion()           # Turn on interactive mode so plot gets updated
fig = plt.figure(1)
plt.subplot(2, 1, 1)
line_1, = plt.plot([0 for i in xrange(0, BLOCKSIZE)])
plt.legend([line_1],['input'])
plt.subplot(2, 1, 2)
line_2, = plt.plot([0 for i in xrange(0, BLOCKSIZE)])
#plt.xlim(0, BLOCKSIZE)
plt.legend([line_2],['AMoutput'])
plt.show()

# Number of blocks in wave file
num_blocks = int(math.floor(LEN/BLOCKSIZE))

# Initialize angle
theta = 0.0

# Block-to-block angle increment
theta_del = (float(BLOCKSIZE*f0)/RATE - math.floor(BLOCKSIZE*f0/RATE)) * 2.0 * math.pi

print('* Playing...')

# Go through wave file 
for i in range(0, num_blocks):

    # Get block of samples from wave file
    input_string = wf.readframes(BLOCKSIZE)     # BLOCKSIZE = number of frames read

    # Convert binary string to tuple of numbers    
    input_tuple = struct.unpack('h' * BLOCKSIZE, input_string)
            # (h: two bytes per sample (WIDTH = 2))

    # Go through block
    for n in range(0, BLOCKSIZE):
        # Amplitude modulation  (f0 Hz cosine)
        output_block[n] = input_tuple[n] * math.cos(2*math.pi*n*f0/RATE + theta)
        # output_block[n] = input_tuple[n] * 1.0  # for no processing
    fft_input = np.fft.fft(input_tuple)/BLOCKSIZE
    fft_output = np.fft.fft(output_block)/BLOCKSIZE
    line_1.set_ydata(abs(fft_input))
    line_2.set_ydata(abs(fft_output))
    plt.subplot(2, 1, 1)
    plt.ylim(0, max(abs(abs(fft_input))))
    plt.subplot(2, 1, 2)
    plt.ylim(0, max(abs(abs(fft_output))))    
    plt.title('Block {0:d}'.format(i))
    plt.pause(0.0001)   
    # Set angle for next block
    theta = theta + theta_del

    # Convert values to binary string
    output_string = struct.pack('h' * BLOCKSIZE, *output_block)

    # Write binary string to audio output stream
    stream.write(output_string)

print('* Done')

stream.stop_stream()
stream.close()
p.terminate()

# Original file by Gerald Schuller, October 2013
