# play_wav_mono.py

import pyaudio
import wave
import struct
import math
import numpy as np
from scipy.signal import butter, freqz, lfilter
from matplotlib import pyplot as plt

from myfunctions import clip16

wave_file_name = 'author.wav'
# wave_file_name = 'sin01_mono.wav'
# wave_file_name = 'sin01_stereo.wav'
wf = wave.open( wave_file_name, 'rb')


b, a = butter(2, [2*500.0/16000.0, 2*1000.0/16000.0], btype = 'bandstop')
print b,a
# Difference equation coefficients
# b0 =  0.008442692929081
# b2 = -0.016885385858161
# b4 =  0.008442692929081

# a0 =  1.000000000000000
# a1 = -3.580673542760982
# a2 =  4.942669993770672
# a3 = -3.114402101627517
# a4 =  0.757546944478829

# Initialization
blocksize = 1024
y = [0.0 for i in xrange(blocksize)]
x = [0.0 for j in xrange(blocksize)]

n = len(y) # length of the signal
k = np.arange(n)
T = n/16000.0
frq = k/T # two sides frequency range
frq = frq[range(n/2)] # one side frequency range

plt.ion()           # Turn on interactive mode so plot gets updated
fig = plt.figure(1)
plt.subplot(3, 1, 1)
line_1, = plt.plot([j-3200 for j in y])
line_2, = plt.plot([i+32000 for i in y])
plt.ylim(-64000, 64000)
plt.xlim(0, blocksize)
plt.xlabel('Time (n)')
plt.legend([line_1, line_2], ['Left channel input', 'Right channel output'])
plt.subplot(3, 1, 2)
line_3, = plt.plot(y)
plt.title('input spectrum')
plt.subplot(3, 1, 3)
line_4, = plt.plot(y)
plt.title('output spectrum')
plt.show()


p = pyaudio.PyAudio()

# Open audio stream
stream = p.open(format      = pyaudio.paInt16,
                channels    = 2,
                rate        = 16000,
                input       = False,
                output      = True )
stream_in = p.open(format      = pyaudio.paInt16,
                   channels    = 1,
                   rate        = 16000,
                   input       = True,
                   output      = False)

# Get first frame
# input_string = wf.readframes(1)

# input_string = wf.readframes(blocksize)
zi = np.array([0, 0, 0, 0])
while True:
    
    input_string = stream_in.read(blocksize)
    # Convert string to number
    input_tuple = struct.unpack('h'*blocksize, input_string)  # One-element tuple
    y, zi = lfilter(b, a, input_tuple, zi = zi)                  # Number

    # Set input to difference equation
    # data = ''
    # for i in xrange(blocksize):
    # Difference equation
        # y[i] = b[0]*x[i+4] + b[1]*x[i+3] + b[2]*x[i+2] + b[3]*x[i+1] + b[4]*x[i] - a[1]*y[i-1] - a[2]*y[i-2] - a[3]*y[i-3] - a[4]*y[i-4] 
        # y[i] = clip16(y[i])
        # data += struct.pack('h', x[i+4])
        # data += struct.pack('h', y[i])
    # print y
    

    Y = np.fft.fft(y)/n
    X = np.fft.fft(input_tuple)/n
    line_1.set_ydata([j-32000 for j in input_tuple])
    line_2.set_ydata([i+32000 for i in y])
    line_3.set_ydata(abs(X))
    line_4.set_ydata(abs(Y))
    plt.subplot(3, 1, 2)
    plt.ylim(0, max(abs(X)))
    plt.subplot(3, 1, 3)
    plt.ylim(0, max(abs(Y)))
    plt.pause(0.0001) 
    # Delays
    # x[0:4] = x[-4:]

    # Compute output value

    # Convert output value to binary string
    data = struct.pack('h'*blocksize, *y)

    # Write binary string to audio stream
    stream.write(data)                     

    # Get next frame
    # input_string = wf.readframes(blocksize)

print("**** Done ****")

stream.stop_stream()
stream.close()
p.terminate()
