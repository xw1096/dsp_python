import pyaudio
import struct
import numpy as np
from scipy import signal
from matplotlib import pyplot as plt

from myfunctions import clip16

N, Wn = signal.cheb2ord(0.45, 0.5, 3, 60)
b, a = signal.cheby2(N, 40, Wn, 'low')

I = 1j
s = np.exp(I*0.5*np.pi*np.array([i for i in xrange(N + 1)]))

b2 = b*s
a2 = a*s

w, h = signal.freqz(b, a)
w2, h2 = signal.freqz(b2, a2)

blocksize = 1024

output = np.array([0.0 for i in xrange(blocksize)])
output_real = np.array([0.0 for i in xrange(blocksize)])
y = np.array([0.0 for i in xrange(N)])
x = np.array([0.0 for i in xrange(N)])

print len(y)
print N
print sum(b2[1:]*x)-sum(a2[1:]*y)
print sum(b2)
