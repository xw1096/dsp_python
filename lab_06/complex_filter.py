import pyaudio
import struct
import numpy as np
from scipy import signal
from matplotlib import pyplot as plt

from myfunctions import clip16

N, Wn = signal.cheb2ord(0.45, 0.5, 3, 60)
b, a = signal.cheby2(N, 40, Wn, 'low')

I = 1j
s = np.exp(I*0.5*np.pi*np.array([i for i in xrange(N + 1)]))

b2 = b*s
a2 = a*s

w, h = signal.freqz(b, a)
w2, h2 = signal.freqz(b2, a2)

blocksize = 128
f0 = 1600
theta = 0.0
theta_del = (float(blocksize*f0)/16000 - np.floor(blocksize*f0/16000)) * 2.0 * np.pi
output_block = np.array([0 for n in xrange(0, blocksize)])

p = pyaudio.PyAudio()
stream = p.open(format      = pyaudio.paInt16,
                channels    = 1,
                rate        = 16000,
                input       = True,
                output      = True )

index = 0
zi = np.zeros(N)
while True:

    input_string = stream.read(blocksize)
    input_tuple = struct.unpack('h'*blocksize, input_string)
    y, zi = signal.lfilter(b2, a2, input_tuple, zi=zi)

    for n in xrange(blocksize):
        output_block[n] = y[n] * np.cos(2*np.pi*n*f0/16000+theta)

    theta = theta + theta_del

    data = struct.pack('h'*blocksize, *output_block.real)
    stream.write(data)

stream.stop_stream()
stream.close()
p.terminate()

