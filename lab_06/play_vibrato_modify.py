# play_vibrato_ver2.py
# Reads a specified wave file (mono) and plays it with a vibrato effect.
# (Sinusoidal time-varying delay)
# This implementation uses a circular buffer with two buffer indices.
# Uses linear interpolation

import time
import pyaudio
import wave
import struct
import math
from myfunctions import clip16


# Read wave file properties
RATE = 16000           # Sampling rate (frames/second)

# Vibrato parameters
f0 = 5
W = 0.2
# W = 0 # for no effct

# OR
# f0 = 20
# ratio = 1.06
# W = (ratio - 1.0) / (2 * math.pi * f0 )
# print W

# Create a buffer (delay line) for past values
buffer_MAX =  1024                          # Buffer length


# Buffer (delay line) indices

buffer = [0.0 for i in range(buffer_MAX)]   # Initialize to zero
n = 0
kr_0 = 0
kw = int(0.5 * buffer_MAX)  # write index (initialize to middle of buffer)

# print('The delay of {0:.3f} seconds is {1:d} samples.'.format(delay_sec, delay_samples))
print 'The buffer is {0:d} samples long.'.format(buffer_MAX)

# Open an output audio stream

def fun(input_string, block_size, time_info, status):
# for n in range(0, LEN):
    global kr_0, kw, n
    # Get sample from wave file
    # input_string = stream_in.read(block_size)
    # Convert string to number
    input_value = struct.unpack('h'*block_size, input_string)
    
    
    output_string = ''
    for i in xrange(block_size):
    # Get previous and next buffer values (since kr is fractional)
        kr_prev_0 = int(math.floor(kr_0))               
        kr_next_0 = kr_prev_0 + 1
        frac_0 = kr_0 - kr_prev_0    # 0 <= frac < 1
        if kr_next_0 >= buffer_MAX:
            kr_next_0 = kr_next_0 - buffer_MAX

    # Compute output value using interpolation
        output_value_0 = (1-frac_0) * buffer[kr_prev_0] + frac_0 * buffer[kr_next_0]

    # Update buffer (pure delay)
        buffer[kw] = input_value[i]

    # Increment read index
        kr_0 = kr_0 + 1 + W * math.sin( 2 * math.pi * f0 * (n*block_size+i) / RATE )
        
        # Note: kr is fractional (not integer!)

    # Ensure that 0 <= kr < buffer_MAX
        if kr_0 >= buffer_MAX:
        # End of buffer. Circle back to front.
            kr_0 = 0


    # Increment write index    
        kw = kw + 1
        if kw == buffer_MAX:
        # End of buffer. Circle back to front.
            kw = 0

    # Clip and convert output value to binary string
        output_string += struct.pack('h', clip16(output_value_0))

    n = n+1
    # Write output to audio stream
    return (output_string, pyaudio.paContinue)


p = pyaudio.PyAudio()
stream = p.open(format      = pyaudio.paInt16,
                channels    = 1,
                rate        = RATE,
                input       = True,
                output      = True,
                stream_callback = fun )


print ('* Playing...')

# Loop through wave file 
stream.start_stream()

# Keep the stream active for 6 seconds by sleeping here
time.sleep(20.0)

print('* Done')

stream.stop_stream()
stream.close()
p.terminate()

