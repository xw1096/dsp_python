# play_delay.py
# Reads a specified wave file (mono) and plays it with a delay.
# This implementation uses a circular buffer.

import pyaudio
import wave
import struct
import math
from myfunctions import clip16

# wavfile = 'author.wav'
# print('Play the wave file %s.' % wavfile)

# Open the wave file
# wf = wave.open( wavfile, 'rb')
wf = wave.open('my_name_saved.wav', 'w')
wf.setnchannels(2)
wf.setsampwidth(2)
wf.setframerate(16000)

# Read the wave file properties
# SIGNAL_LEN  = wf.getnframes()   # Signal length
# CHANNELS = wf.getnchannels()    # Number of channels
# WIDTH = wf.getsampwidth()       # Number of bytes per sample
# RATE = wf.getframerate()        # Sampling rate (frames/second)
RATE = 16000
# print('The file has %d channel(s).'            % CHANNELS)
# print('The frame rate is %d frames/second.'    % RATE)
# print('The file has %d frames.'                % SIGNAL_LEN)
# print('There are %d bytes per sample.'         % WIDTH)

# Set parameters of delay system
Gdp = 1.0           # direct-path gain
Gff = 0.8           # feed-forward gain
delay_sec_1 = 1.0    # 50 milliseconds
delay_sec_2 = 0.02
delay_samples_1 = int( math.floor( RATE * delay_sec_1 ) ) 
delay_samples_2 = int(math.floor(RATE*delay_sec_2))
print('The left delay of {0:.3f} seconds is {1:d} samples.\nThe right delay of {2:.3f} seconds is {3:d} samples'.format(delay_sec_1, delay_samples_1, delay_sec_2, delay_samples_2))

# Create a buffer to store past values. Initialize to zero.
BUFFER_LEN_1 = delay_samples_1   # set the length of buffer
BUFFER_LEN_2 = delay_samples_2
buffer_1 = [ 0 for i in range(BUFFER_LEN_1) ]
buffer_2 = [0 for i in range(BUFFER_LEN_2)]    

# Open an output audio stream
p = pyaudio.PyAudio()
stream = p.open(format      = pyaudio.paInt16,
                channels    = 2,
                rate        = RATE,
                input       = False,
                output      = True )

stream_in = p.open(format      = pyaudio.paInt16,
                   channels    = 1,
                   rate        = RATE,
                   input       = True,
                   output      = False )
# Get first frame (sample)
# input_string = wf.readframes(1)

k_1 = 0       # buffer index (circular index)
k_2 = 0

print ('* Start...')

while True:

    # Convert string to number
    input_string = stream_in.read(1)
    input_value = struct.unpack('h', input_string)[0]
    # Compute output value
    output_value_1 = Gdp * input_value + Gff * buffer_1[k_1]
    output_value_1 = clip16(output_value_1)
    output_value_2 = Gdp*input_value + Gff*buffer_2[k_2]
    output_value_2 = clip16(output_value_2)

    # Update buffer
    buffer_1[k_1] = input_value
    buffer_2[k_2] = input_value
    k_1 = k_1 + 1
    k_2 = k_2 +1
    if k_1 >= BUFFER_LEN_1:
        k_1 = 0
    if k_2 >= BUFFER_LEN_2:
        k_2 = 0


    # Convert output value to binary string
    output_string = struct.pack('h', output_value_1)
    output_string += struct.pack('h', output_value_2)

    # Write output value to audio stream
    stream.write(output_string)
    wf.writeframes(output_string)
    
    # Get next frame (sample)
    # input_string = wf.readframes(1)     

# end_echo_1 = buffer_1[k_1:] + buffer_1[0:k_1]
# end_echo_2 = buffer_2[k_2:] + buffer_2[0:k_2]
# end_echo_2 = end_echo_2 + [0 for i in range(BUFFER_LEN_1-BUFFER_LEN_2)]

# while end_echo_1:
#     output_string = struct.pack('h', clip16(end_echo_1[0]))
#     output_string += struct.pack('h', clip16(end_echo_2[0]))
#     end_echo_1.pop(0)
#     end_echo_2.pop(0)
#     stream.write(output_string)

print('* Done')

stream.stop_stream()
stream.close()
stream_in.stop_stream()
stream_in.close()
p.terminate()
