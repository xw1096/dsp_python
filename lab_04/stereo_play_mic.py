import pyaudio
import struct
import math

from myfunctions import clip16

WIDTH = 2
CHANNELS = 2
RATE = 16000
DURATION = 10

N = DURATION*RATE

# Difference equation coefficients
b0 =  0.008442692929081
b2 = -0.016885385858161
b4 =  0.008442692929081

# a0 =  1.000000000000000
a1 = -3.580673542760982
a2 =  4.942669993770672
a3 = -3.114402101627517
a4 =  0.757546944478829

# Initialization
x1 = 0.0
x2 = 0.0
x3 = 0.0
x4 = 0.0
y1 = 0.0
y2 = 0.0
y3 = 0.0
y4 = 0.0

p = pyaudio.PyAudio()

stream_input = p.open(format = p.get_format_from_width(WIDTH),
                channels = CHANNELS/2,
                rate = RATE,
                input = True,
                output = False)
stream_output = p.open(format = p.get_format_from_width(WIDTH),
                channels = CHANNELS,
                rate = RATE,
                input = False,
                output = True)

print '**** start ****'

while True:
    
    input_string = stream_input.read(1)
    input_tuple = struct.unpack('h', input_string)
    input_value = input_tuple[0]
    x0 = input_value
    y0 = b0 * x0 + b2 * x2 + b4 * x4 - a1 * y1 - a2 * y2 - a3 * y3 - a4 * y4
    x4 = x3
    x3 = x2
    x2 = x1
    x1 = x0
    y4 = y3
    y3 = y2
    y2 = y1
    y1 = y0
    output_value = clip16(y0)
    output_string = struct.pack('h', output_value)
    output_string += input_string
    stream_output.write(output_string)

print '**** done ****'

stream.stop_stream()
stream.close()
p.terminate()
