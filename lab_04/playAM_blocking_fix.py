# playAM_blocking_fix.py
# Play a mono wave file with amplitude modulation. 
# This implementation reads and plays a block at a time (blocking)
# and corrects for block-to-block angle mismatch.
# Assignment: modify this file so it works for both mono and stereo wave files
#  (where does this file have an error when wave file is stereo and why? )

# f0 = 0      # Normal audio
f0 = 400    # 'Duck' audio
f1 = 1600    
BLOCKSIZE = 64      # Number of frames per block

import pyaudio
import struct
import wave
import math

# Open wave file (mono)
# wave_file_name = 'author.wav'
# wave_file_name = 'sin01_mono.wav'
# wave_file_name = 'sin01_stereo.wav'
# wf = wave.open( wave_file_name, 'rb')
# RATE = wf.getframerate()
# WIDTH = wf.getsampwidth()
# LEN = wf.getnframes() 
# CHANNELS = wf.getnchannels() 
wf = wave.open('AMsaved.wav', 'w')
wf.setnchannels(2)
wf.setsampwidth(2)
wf.setframerate(16000)
RATE = 16000
# print 'The sampling rate is {0:d} samples per second'.format(RATE)
# print 'Each sample is {0:d} bytes'.format(WIDTH)
# print 'The signal is {0:d} samples long'.format(LEN)
# print 'The signal has {0:d} channel(s)'.format(CHANNELS)
LEN = 16000.0*20.0
# Open audio stream
p = pyaudio.PyAudio()
stream = p.open(format = pyaudio.paInt16,
                channels = 2,
                rate = 16000,
                input = False,
                output = True)
stream_in = p.open(format = pyaudio.paInt16,
                   channels = 1,
                   rate = 16000,
                   input = True,
                   output = False)
# Create block (initialize to zero)
output_block_1 = [0 for n in range(0, BLOCKSIZE)]
output_block_2 = [0 for n in range(0, BLOCKSIZE)]
# Number of blocks in wave file
num_blocks = int(math.floor(LEN/BLOCKSIZE))

# Initialize angle
theta_1 = 0.0
theta_2 = 0.0
# Block-to-block angle increment
theta_del_1 = (float(BLOCKSIZE*f0)/RATE - math.floor(BLOCKSIZE*f0/RATE)) * 2.0 * math.pi
theta_del_2 = (float(BLOCKSIZE*f1)/RATE - math.floor(BLOCKSIZE*f1/RATE)) * 2.0 * math.pi
print('* Playing...')

# Go through wave file 
for i in range(0, num_blocks):

    # Get block of samples from wave file
    input_string = stream_in.read(BLOCKSIZE)     # BLOCKSIZE = number of frames read

    # Convert binary string to tuple of numbers    
    input_tuple = struct.unpack('h' * BLOCKSIZE, input_string)
            # (h: two bytes per sample (WIDTH = 2))
    output_string = ''
    # Go through block
    for n in range(0, BLOCKSIZE):
        # Amplitude modulation  (f0 Hz cosine)
        output_block_1 = input_tuple[n] * math.cos(2*math.pi*n*f0/RATE + theta_1)
        # output_block[n] = input_tuple[n] * 1.0  # for no processing
        output_block_2 = input_tuple[n] * math.cos(2*math.pi*n*f1/RATE + theta_1)
        output_string += struct.pack('h', output_block_1)
        output_string += struct.pack('h', output_block_2)
    theta_1 = theta_1 + theta_del_1
    theta_2 = theta_2 +theta_del_2
   
    # Write binary string to audio output stream
    stream.write(output_string)
    wf.writeframes(output_string)

print('* Done')

stream.stop_stream()
stream.close()
p.terminate()

# Original file by Gerald Schuller, October 2013
