function gui_demo

N = 4000;
n = 1:N;
x = zeros(N, 1);
x(1) = 1;              % Input signal

figure(1)
clf
subplot(1, 3, 1)
zplane(0, 1);
title('pole/zero diagram')
subplot(1, 3, 2)
plot(n, x);
title('impulse response')
subplot(1, 3, 3)
plot(n, x)
title('frequency response' )


drawnow;

slider_radiu = uicontrol('Style', 'slider', ...
    'Tag', 'slider1',...
    'Min', 0, 'Max', 1,...
    'Value', 1, ...
    'SliderStep', [0.001 0.001], ...
    'Position', [5 5 200 20], ...          
    'Callback',  {@fun1, x}    );

slider_angle = uicontrol('Style', 'slider', ...
    'Tag', 'slider2',...
    'Min', 0, 'Max', 1,...
    'Value', 1, ...
    'SliderStep', [0.02 0.05], ...
    'Position', [5 30 200 20], ...         
    'Callback',  {@fun2, x}    );

txt1 = uicontrol('Style', 'text',...
    'Tag', 'txt1', ...
    'Position', [205 5 200 20],...
    'String', 'radiu = 1');

txt2 = uicontrol('Style', 'text',...
    'Tag', 'txt2', ...
    'Position', [205 30 200 20],...
    'String', 'angle = 1');

end


function fun1(hObject, ~, x)

share = findobj('Tag', 'slider2');
angle = get(share, 'Value');
angle = pi*angle;
radiu = get(hObject, 'Value');

a = find_a(radiu, angle);

y = filter(1, a, x);
[h, w] = freqz(1, a);

subplot(1, 3, 1)
zplane(1, a)
title('pole/zero diagram')
subplot(1, 3, 2)
plot(y)
title('impulse response')
subplot(1, 3, 3)
plot(w, abs(h))
title('frequency response' )

txt = findobj('Tag', 'txt1');
set(txt, 'string', sprintf('radiu = %.3f', radiu))

end

function fun2(hObject, ~, x)

share = findobj('Tag', 'slider1');
radiu = get(share, 'Value');
angle = get(hObject, 'Value');
angle = pi*angle;

a = find_a(radiu, angle);

y = filter(1, a, x);
[h, w] = freqz(1, a);

subplot(1, 3, 1)
zplane(1, a)
title('pole/zero diagram')
subplot(1, 3, 2)
plot(y)
title('impulse response')
subplot(1, 3, 3)
plot(w, abs(h))
title('frequency response' )

txt = findobj('Tag', 'txt2');
set(txt, 'string', sprintf('angle = %.3f', angle))

end

function a = find_a(radiu, angle)
a = [1 -2*radiu*cos(angle) radiu^2];
end