from math import cos, pi
import pyaudio
import struct

Fs = 8000

T = 1
N = T*Fs

a1 = -1.8999
a2 = 0.9977

y1 = 0.0
y2 = 0.0

gain = input('please input yout gain')
if gain > 2**15 - 1:
    print 'the gain will set to maximum allowed value'
    gain = 2**15 -1

p = pyaudio.PyAudio()
stream = p.open(format = pyaudio.paInt16,
                channels = 1,
                rate = Fs,
                input = False,
                output = True)

for n in range(0, N):
    if n == 0:
        x0 = 1.0
    else:
        x0 = 0.0

    y0 = x0 - a1*y1 - a2*y2
    y2 = y1
    y1 = y0
    print y0

    out = gain*y0
    if out > 2**15 - 1:
        out = 2**15 - 1
    elif out < -2**15:
        out = -2**15
    str_out = struct.pack('h', out)
    stream.write(str_out)

print('* done *')

stream.stop_stream()
stream.close()
p.terminate()
