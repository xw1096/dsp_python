# In this code I will cascade two two second-order filters to create a impulse response with rise and decay-time



from math import cos, pi
import pyaudio
import struct
import numpy as np

Fs = 8000
sound_f = 400
om = 2.0*pi*float(sound_f)/Fs
Ta = 1.0
r = 0.01**(1.0/(Ta*Fs))
#r1 = 0.5
#r2 = 0.5

T = 1
N = T*Fs

a11 = -2*r*cos(om)
a12 = r**2
a21 = -2*r*cos(om)
a22 = r**2
r = np.poly1d([1, a11, a12])*np.poly1d([1, a21, a22])
print r

y11 = 0.0
y12 = 0.0
y21 = 0.0
y22 = 0.0

gain = input('please input yout gain')
if gain > 2**15 - 1:
    print 'the gain will set to maximum allowed value'
    gain = 2**15 -1

p = pyaudio.PyAudio()
stream = p.open(format = pyaudio.paInt16,
                channels = 1,
                rate = Fs,
                input = False,
                output = True)

for n in range(0, 10*N):
    if n == 0:
        x0 = 1.0
    else:
        x0 = 0.0

    y10 = x0 - a11*y11 - a12*y12
    y12 = y11
    y11 = y10

    y20 = y10 - a21*y21 - a22*y22
    y22 = y21
    y21 = y20

    #y0 = x0 - r[1]*y1 - r[2]*y2 - r[3]*y3 - r[4]*y4
    #y4 = y3
    #y3 = y2
    #y2 = y1
    #y1 = y0
    
    print y20
    

    out = gain*y20
    if out > 2**15 - 1:
        out = 2**15 - 1
    elif out < -2**15:
        out = -2**15
    str_out = struct.pack('h', out)
    stream.write(str_out)

print('* done *')

stream.stop_stream()
stream.close()
p.terminate()
