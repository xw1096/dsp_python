% In this matlab code I will use function conv() to combine two two second-order filters into  one fourth order filter


sample_rate = 8000;
sound_f = 400;
norm_f = sound_f/sample_rate;
om = 2*pi*norm_f;
r1 = 0.999;
r2 = 0.99;
a1 = [1 -2*r1*cos(om) r1^2];
a2 = [1 -2*r2*cos(om) r2^2];
f1 = dfilt.df1(1, a1);
f2 = dfilt.df1(1, a2);
fca = dfilt.cascade(f1, f2);
sample = zeros(8000, 1);
sample(1) = 1;
%plot(filter(fca, sample))
sound(filter(fca,sample),sample_rate)
y1 = 0.0;
y2 = 0.0;
y3 = 0.0;
y4 = 0.0;
a = conv(a1, a2);
y0 = zeros(2000, 1);
figure(1)
hold on
for k=1:2000
    y0(k) = sample(k)-a(2)*y1-a(3)*y2-a(4)*y3-a(5)*y4;
    y4 = y3;
    y3 = y2;
    y2 = y1;
    y1 = y0(k);
end
plot(y0)
