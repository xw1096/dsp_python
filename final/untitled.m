infile = 'Toms_diner.wav';
[ audio_in, fs] = audioread(infile);
%Prompt for and store 'Damping', 'Min_Cutoff' and 'Max_Cutoff' and 'Width'

damping = 0.05;
width = 5000;
min_cutoff = 500;
max_cutoff = 5000;
center_freq = width/fs;
cutoff_freq=min_cutoff:center_freq:max_cutoff;
while(length(cutoff_freq) < length(audio_in) )
cutoff_freq = [ cutoff_freq (max_cutoff:-center_freq:min_cutoff) ];
cutoff_freq = [ cutoff_freq (min_cutoff:center_freq:max_cutoff) ];
end
cutoff_freq = cutoff_freq(1:length(audio_in));
F1 = 2*sin((pi*cutoff_freq(1))/fs);
Q1 = 2*damping;
highpass=zeros(size(audio_in));
bandpass=zeros(size(audio_in));
lowpass=zeros(size(audio_in));
highpass(1) = audio_in(1);
bandpass(1) = F1*highpass(1);
lowpass(1) = F1*bandpass(1);
for n=2:length(audio_in),
highpass(n) = audio_in(n) - lowpass(n-1) - Q1*bandpass(n-1);
bandpass(n) = F1*highpass(n) + bandpass(n-1);
lowpass(n) = F1*bandpass(n) + lowpass(n-1);
F1 = 2*sin((pi*cutoff_freq(n))/fs);
end
normed = bandpass./max(max(abs(bandpass)));
audiowrite('wah wahed.wav', normed, fs);
sound (normed, fs);