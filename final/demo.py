import pygame, sys
import pyaudio
import scipy as sp
import struct
from scipy import signal

from myfunctions import clip16

def delay():

    RATE = 44100
    WIDTH = 2
    CHANNELS = 1
    BLOCKSIZE = 1024

    Gdp = 1.0
    Gff = 0.0
    Gfb = 0.0
    delay_sec = 0.1
    delay_samples = int(sp.floor(RATE*delay_sec))

    p = pyaudio.PyAudio()
    stream = p.open(format      = p.get_format_from_width(WIDTH),
                    channels    = CHANNELS,
                    rate        = RATE,
                    input       = True,
                    output      = True,
                    frames_per_buffer = 2048 )
    test = False
    output_value = [0.0 for i in xrange(0, BLOCKSIZE)]
    delay_buffer = [0.0 for i in xrange(0, delay_samples)]
    buffer_index = 0

    print "***start playing delay sound effect***"

    while not test:
        
        input_string = stream.read(BLOCKSIZE)
        input_value = struct.unpack('h'*BLOCKSIZE, input_string)

        for n in xrange(0, BLOCKSIZE):

            output_value[n] = Gdp*input_value[n] + Gff*delay_buffer[buffer_index]
            output_value[n] = clip16(output_value[n])

            delay_buffer[buffer_index] = input_value[n] + Gfb*delay_buffer[buffer_index]
            buffer_index = buffer_index + 1
            if buffer_index >= delay_samples:
            
                buffer_index = 0
            
        output_string = struct.pack('h'*BLOCKSIZE, *output_value)
        stream.write(output_string)

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_q:

                    test = True         

    print('***Done***')

    stream.stop_stream()
    stream.close()
    p.terminate()

def flanging():

    RATE = 44100
    WIDTH = 2
    CHANNELS = 1
    BLOCKSIZE = 1024

    f0 = 1
    w = 0.9

    p = pyaudio.PyAudio()
    stream = p.open(format      = p.get_format_from_width(WIDTH),
                    channels    = CHANNELS,
                    rate        = RATE,
                    input       = True,
                    output      = True, 
                    frames_per_buffer = 2048)
    test = False
    buffer_len = 1024
    output_value = [0.0 for i in xrange(0, BLOCKSIZE)]
    delay_buffer = [0.0 for i in xrange(0, buffer_len)]
    buffer_read = 0
    buffer_write = int(0.5*buffer_len)
    repeat = 0

    print "start playing flanging sound effect"

    while not test:
        
        input_string = stream.read(BLOCKSIZE)
        input_value = struct.unpack('h'*BLOCKSIZE, input_string)

        for n in xrange(0, BLOCKSIZE):

            output_value[n] = input_value[n] + delay_buffer[int(buffer_read)]
            output_value[n] = clip16(output_value[n])

            delay_buffer[buffer_write] = input_value[n]
            buffer_read = buffer_read + 1 + w*sp.sin(2*sp.pi*f0*(repeat*BLOCKSIZE+n)/RATE)
            if buffer_read >= buffer_len:
            
                buffer_read = buffer_read - buffer_len
            buffer_write = buffer_write + 1
            if buffer_write == buffer_len:
            
                buffer_write = 0   
        repeat = repeat + 1

        output_string = struct.pack('h'*BLOCKSIZE, *output_value)
        stream.write(output_string)

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_q:

                    test = True     
                if event.key == pygame.K_w:

                    f0 = f0 + 1   
                if event.key == pygame.K_s:

                    f0 = f0 - 1

    print('***Done***')

    stream.stop_stream()
    stream.close()
    p.terminate()

def robotization():

    RATE = 44100
    WIDTH = 2
    CHANNELS = 1
    BLOCKSIZE = 1024

    win_size = 1024
    window = sp.array([sp.cos(sp.pi*(i+0.5)/win_size - sp.pi/2) for i in xrange(win_size)])

    p = pyaudio.PyAudio()
    stream = p.open(format = p.get_format_from_width(WIDTH),
                    channels = CHANNELS,
                    rate = RATE,
                    input  =True,
                    output = True,
                    frames_per_buffer = 2048)
    test = False
    temp_pre = sp.array([0.0 for i in xrange(0, win_size)])
    temp_now = sp.array([0.0 for i in xrange(0, win_size)])

    print "start playing robotization sound effect"
    
    input_string = stream.read(win_size/2)
    input_value =  sp.array(struct.unpack('h'*(win_size/2), input_string))
    temp_pre[win_size/2:] = sp.copy(input_value)   
    
    fft_pre = sp.absolute(sp.fft(window*sp.array(temp_pre)))

    while not test:

        input_string = stream.read(win_size/2)
        input_value = sp.array(struct.unpack('h'*(win_size/2), input_string))
        temp_now[0:win_size/2] = sp.copy(temp_pre[win_size/2:])
        temp_now[win_size/2:] = sp.copy(input_value)

        fft_now = sp.absolute(sp.fft(window*sp.array(temp_now)))
        output_value = (window*sp.ifft(fft_pre))[win_size/2:] + (window*sp.ifft(fft_now))[0:win_size/2]
        
        fft_pre = sp.copy(fft_now)
        temp_pre = sp.copy(temp_now)
    
        output_string = struct.pack('h'*(win_size/2), *output_value)
    
        stream.write(output_string)     

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_q:

                    test = True  

    print('***Done***')

    stream.stop_stream()
    stream.close()
    p.terminate()

def wahwah():

    RATE = 44100
    WIDTH = 2
    CHANNELS = 1
    BLOCKSIZE = 1024

    damp = 0.05
    width = 2000.0
    min_cutoff = 500.0
    max_cutoff = 5000.0
    center_freq = width/RATE
    osc_freq = min_cutoff

    p = pyaudio.PyAudio()
    stream = p.open(format = p.get_format_from_width(WIDTH),
                    channels = CHANNELS,
                    rate = RATE,
                    input  =True,
                    output = True,
                    frames_per_buffer = 2048)
    test = False
    highpass = [0.0 for i in xrange(BLOCKSIZE)]
    bandpass = [0.0 for i in xrange(BLOCKSIZE)]
    lowpass = [0.0 for i in xrange(BLOCKSIZE)]   

    print "start playing wahwah sound effect"
    
    f1 = 2*sp.sin(sp.pi*osc_freq/RATE)
    q1 = 2*damp
    while not test:

        input_string = stream.read(BLOCKSIZE)
        input_value = struct.unpack('h'*BLOCKSIZE, input_string)

        highpass[0] = input_value[0]
        bandpass[0] = f1*highpass[0]
        lowpass[0] = f1*bandpass[0]

        for n in xrange(1, BLOCKSIZE):
  
            if osc_freq >= max_cutoff:

                center_freq = -abs(center_freq)

            if osc_freq <= min_cutoff:
 
                center_freq = abs(center_freq)
            f1 = 2*sp.sin(sp.pi*osc_freq/RATE)
            osc_freq = osc_freq + center_freq

            highpass[n] = input_value[n] - lowpass[n - 1] - q1*bandpass[n - 1]
            bandpass[n] = f1*highpass[n] + bandpass[n - 1]
            lowpass[n] = f1*bandpass[n] + lowpass[n - 1]

        output_string = struct.pack('h'*BLOCKSIZE, *bandpass)
        stream.write(output_string)

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_q:

                    test = True  

    print('***Done***')

    stream.stop_stream()
    stream.close()
    p.terminate()
if __name__ == "__main__":
    pygame.init()
    screen = pygame.display.set_mode((400, 400))

    test = False

    while not test:
        for event in pygame.event.get():
    #event = pygame.event.get()
            font = pygame.font.Font("FreeSansBold.ttf", 50)
            text = font.render("test", 1, (0, 255, 0))
            screen.blit(text, (50, 50))
            pygame.display.update()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    delay()
                if event.key == pygame.K_w:
                    flanging()
                if event.key == pygame.K_e:
                    robotization()
                if event.key == pygame.K_r:
                    wahwah()
                if event.key == pygame.K_x:
                    test = True

    print "***done***"
