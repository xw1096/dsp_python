import pygame
import pyaudio
import scipy as sp
import struct
from myfunctions import clip16, clip16_arr

pygame.init()
screen = pygame.display.set_mode((800, 400))

def delay():

    RATE = 44100
    WIDTH = 2
    CHANNELS = 1
    BLOCKSIZE = 1024

    Gdp = 1.0
    Gff = 0.0
    Gfb = 0.0
    delay_sec = 0.1
    delay_samples = int(sp.floor(RATE*delay_sec))

    p = pyaudio.PyAudio()
    stream = p.open(format      = p.get_format_from_width(WIDTH),
                    channels    = CHANNELS,
                    rate        = RATE,
                    input       = True,
                    output      = True,
                    frames_per_buffer = 2048 )
    test = False
    output_value = [0.0 for i in xrange(0, BLOCKSIZE)]
    delay_buffer = [0.0 for i in xrange(0, delay_samples)]
    buffer_index = 0

    print "***start playing delay sound effect***"

    while not test:

        input_string = stream.read(BLOCKSIZE)
        input_value = struct.unpack('h'*BLOCKSIZE, input_string)

        for n in xrange(0, BLOCKSIZE):

            output_value[n] = Gdp*input_value[n] + Gff*delay_buffer[buffer_index]
            output_value[n] = clip16(output_value[n])

            delay_buffer[buffer_index] = input_value[n] + Gfb*delay_buffer[buffer_index]
            buffer_index = buffer_index + 1
            if buffer_index >= delay_samples:

                buffer_index = 0

        output_string = struct.pack('h'*BLOCKSIZE, *output_value)
        stream.write(output_string)

        mouse = pygame.mouse.get_pos()
        if 125 < mouse[0] < 125 + 50 and 107 < mouse[1] < 107 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (125, 107, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(delay_sec), 1, (0, 0, 0))
            screen.blit(text,(180, 100))
            pygame.display.update()

            delay_sec = min(delay_sec + 0.1, 1.0)
            text = font.render(str(delay_sec), 1, (0, 255, 0))
            screen.blit(text,(180, 100))
            pygame.display.update()
            delay_samples = int(sp.floor(RATE*delay_sec))
            delay_buffer = [0.0 for i in xrange(0, delay_samples)]
            buffer_index = 0
        else:
            pygame.draw.rect(screen, (0, 255, 0), (125, 107, 50, 20))
            pygame.display.update()
        if 195 < mouse[0] < 195 + 50 and 157 < mouse[1] < 157 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (195, 157, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(Gff), 1, (0, 0, 0))
            screen.blit(text,(250, 150))
            pygame.display.update()

            Gff = min(Gff + 0.1, 0.5)
            text = font.render(str(Gff), 1, (0, 255, 0))
            screen.blit(text,(250, 150))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (195, 157, 50, 20))
            pygame.display.update()
        if 165 < mouse[0] < 165 + 50 and 207 < mouse[1] < 207 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (165, 207, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(Gfb), 1, (0, 0, 0))
            screen.blit(text,(220, 200))
            pygame.display.update()

            Gfb = min(Gfb + 0.1, 0.5)
            text = font.render(str(Gfb), 1, (0, 255, 0))
            screen.blit(text,(220, 200))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (165, 207, 50, 20))
            pygame.display.update()
        if 215 < mouse[0] < 215 + 50 and 107 < mouse[1] < 107 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (215, 107, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(delay_sec), 1, (0, 0, 0))
            screen.blit(text,(180, 100))
            pygame.display.update()

            delay_sec = max(delay_sec - 0.1, 0.1)
            text = font.render(str(delay_sec), 1, (0, 255, 0))
            screen.blit(text,(180, 100))
            pygame.display.update()
            delay_samples = int(sp.floor(RATE*delay_sec))
            delay_buffer = [0.0 for i in xrange(0, delay_samples)]
            buffer_index = 0
        else:
            pygame.draw.rect(screen, (0, 255, 0), (215, 107, 50, 20))
            pygame.display.update()
        if 285 < mouse[0] < 285 + 50 and 157 < mouse[1] < 157 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (285, 157, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(Gff), 1, (0, 0, 0))
            screen.blit(text,(250, 150))
            pygame.display.update()

            Gff = max(Gff - 0.1, 0.0)
            text = font.render(str(Gff), 1, (0, 255, 0))
            screen.blit(text,(250, 150))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (285, 157, 50, 20))
            pygame.display.update()
        if 255 < mouse[0] < 255 + 50 and 207 < mouse[1] < 207 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (255, 207, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(Gfb), 1, (0, 0, 0))
            screen.blit(text,(220, 200))
            pygame.display.update()

            Gfb = max(Gfb - 0.1, 0.0)
            text = font.render(str(Gfb), 1, (0, 255, 0))
            screen.blit(text,(220, 200))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (255, 207, 50, 20))
            pygame.display.update()

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_x:

                    test = True

    print('***Done***')

    stream.stop_stream()
    stream.close()
    p.terminate()

def flanging():

    RATE = 44100
    WIDTH = 2
    CHANNELS = 1
    BLOCKSIZE = 1024

    f0 = 1.0
    w = 0.1

    p = pyaudio.PyAudio()
    stream = p.open(format      = p.get_format_from_width(WIDTH),
                    channels    = CHANNELS,
                    rate        = RATE,
                    input       = True,
                    output      = True,
                    frames_per_buffer = 2048)
    test = False
    buffer_len = 1024
    output_value = [0.0 for i in xrange(0, BLOCKSIZE)]
    delay_buffer = [0.0 for i in xrange(0, buffer_len)]
    buffer_read = 0
    buffer_write = int(0.5*buffer_len)
    repeat = 0

    print "start playing flanging sound effect"

    while not test:

        input_string = stream.read(BLOCKSIZE)
        input_value = struct.unpack('h'*BLOCKSIZE, input_string)

        for n in xrange(0, BLOCKSIZE):

            output_value[n] = input_value[n] + delay_buffer[int(buffer_read)]
            output_value[n] = clip16(output_value[n])

            delay_buffer[buffer_write] = input_value[n]
            buffer_read = buffer_read + 1 + w*sp.sin(2*sp.pi*f0*(repeat*BLOCKSIZE+n)/RATE)
            if buffer_read >= buffer_len:

                buffer_read = buffer_read - buffer_len
            buffer_write = buffer_write + 1
            if buffer_write == buffer_len:

                buffer_write = 0
        repeat = repeat + 1

        output_string = struct.pack('h'*BLOCKSIZE, *output_value)
        stream.write(output_string)

        mouse = pygame.mouse.get_pos()
        if 80 < mouse[0] < 80 + 50 and 107 < mouse[1] < 107 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (80, 107, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(f0), 1, (0, 0, 0))
            screen.blit(text,(135, 100))
            pygame.display.update()

            f0 = min(f0 + 0.1, 10.0)
            text = font.render(str(f0), 1, (0, 255, 0))
            screen.blit(text,(135, 100))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (80, 107, 50, 20))
            pygame.display.update()
        if 180 < mouse[0] < 180 + 50 and 107 < mouse[1] < 107 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (180, 107, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(f0), 1, (0, 0, 0))
            screen.blit(text,(135, 100))
            pygame.display.update()

            f0 = max(f0 - 0.1, 1.0)
            text = font.render(str(f0), 1, (0, 255, 0))
            screen.blit(text,(135, 100))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (180, 107, 50, 20))
            pygame.display.update()
        if 80 < mouse[0] < 80 + 50 and 157 < mouse[1] < 157 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (80, 157, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(w), 1, (0, 0, 0))
            screen.blit(text,(135, 150))
            pygame.display.update()

            w = min(w + 0.1, 0.9)
            text = font.render(str(w), 1, (0, 255, 0))
            screen.blit(text,(135, 150))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (80, 157, 50, 20))
            pygame.display.update()
        if 170 < mouse[0] < 170 + 50 and 157 < mouse[1] < 157 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (170, 157, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(w), 1, (0, 0, 0))
            screen.blit(text,(135, 150))
            pygame.display.update()

            w = max(w - 0.1, 0.1)
            text = font.render(str(w), 1, (0, 255, 0))
            screen.blit(text,(135, 150))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (170, 157, 50, 20))
            pygame.display.update()

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_x:

                    test = True

    print('***Done***')

    stream.stop_stream()
    stream.close()
    p.terminate()

def robotization():

    RATE = 44100
    WIDTH = 2
    CHANNELS = 1
    BLOCKSIZE = 1024

    win_size = 1024
    window = sp.array([sp.cos(sp.pi*(i+0.5)/win_size - sp.pi/2) for i in xrange(win_size)])

    p = pyaudio.PyAudio()
    stream = p.open(format = p.get_format_from_width(WIDTH),
                    channels = CHANNELS,
                    rate = RATE,
                    input  =True,
                    output = True,
                    frames_per_buffer = 2048)
    test = False
    temp_pre = sp.array([0.0 for i in xrange(0, win_size)])
    temp_now = sp.array([0.0 for i in xrange(0, win_size)])

    print "start playing robotization sound effect"

    input_string = stream.read(win_size/2)
    input_value =  sp.array(struct.unpack('h'*(win_size/2), input_string))
    temp_pre[win_size/2:] = sp.copy(input_value)

    fft_pre = sp.absolute(sp.fft(window*sp.array(temp_pre)))

    while not test:

        input_string = stream.read(win_size/2)
        input_value = sp.array(struct.unpack('h'*(win_size/2), input_string))
        temp_now[0:win_size/2] = sp.copy(temp_pre[win_size/2:])
        temp_now[win_size/2:] = sp.copy(input_value)

        fft_now = sp.absolute(sp.fft(window*sp.array(temp_now)))
        output_value = (window*sp.ifft(fft_pre))[win_size/2:] + (window*sp.ifft(fft_now))[0:win_size/2]

        fft_pre = sp.copy(fft_now)
        temp_pre = sp.copy(temp_now)

        output_string = struct.pack('h'*(win_size/2), *output_value)

        stream.write(output_string)

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_x:

                    test = True

    print('***Done***')

    stream.stop_stream()
    stream.close()
    p.terminate()

def wahwah():

    RATE = 44100
    WIDTH = 2
    CHANNELS = 1
    BLOCKSIZE = 1024

    damp = 0.05
    width = 500.0
    min_cutoff = 100.0
    max_cutoff = 5000.0
    center_freq = width/RATE
    osc_freq = min_cutoff

    p = pyaudio.PyAudio()
    stream = p.open(format = p.get_format_from_width(WIDTH),
                    channels = CHANNELS,
                    rate = RATE,
                    input  =True,
                    output = True,
                    frames_per_buffer = 2048)
    test = False
    highpass = [0.0 for i in xrange(BLOCKSIZE)]
    bandpass = [0.0 for i in xrange(BLOCKSIZE)]
    lowpass = [0.0 for i in xrange(BLOCKSIZE)]

    print "start playing wahwah sound effect"

    f1 = 2*sp.sin(sp.pi*osc_freq/RATE)
    q1 = 2*damp
    while not test:

        input_string = stream.read(BLOCKSIZE)
        input_value = struct.unpack('h'*BLOCKSIZE, input_string)

        highpass[0] = input_value[0]
        bandpass[0] = f1*highpass[0]
        lowpass[0] = f1*bandpass[0]

        for n in xrange(1, BLOCKSIZE):

            if osc_freq >= max_cutoff:

                center_freq = -abs(center_freq)

            if osc_freq <= min_cutoff:

                center_freq = abs(center_freq)
            f1 = 2*sp.sin(sp.pi*osc_freq/RATE)
            osc_freq = osc_freq + center_freq

            highpass[n] = input_value[n] - lowpass[n - 1] - q1*bandpass[n - 1]
            bandpass[n] = f1*highpass[n] + bandpass[n - 1]
            lowpass[n] = f1*bandpass[n] + lowpass[n - 1]

        bandpass = clip16_arr(bandpass)
        output_string = struct.pack('h'*BLOCKSIZE, *bandpass)
        stream.write(output_string)

        mouse = pygame.mouse.get_pos()
        if 80 < mouse[0] < 80 + 50 and 107 < mouse[1] < 107 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (80, 107, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(int(width)), 1, (0, 0, 0))
            screen.blit(text,(135, 100))
            pygame.display.update()

            width = min(width + 100.0, 1500.0)
            text = font.render(str(int(width)), 1, (0, 255, 0))
            screen.blit(text,(135, 100))
            pygame.display.update()
            center_freq = width/RATE
        else:
            pygame.draw.rect(screen, (0, 255, 0), (80, 107, 50, 20))
            pygame.display.update()
        if 185 < mouse[0] < 185 + 50 and 107 < mouse[1] < 107 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (185, 107, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(int(width)), 1, (0, 0, 0))
            screen.blit(text,(135, 100))
            pygame.display.update()

            width = max(width - 100.0, 500.0)
            text = font.render(str(int(width)), 1, (0, 255, 0))
            screen.blit(text,(135, 100))
            pygame.display.update()
            center_freq = width/RATE
        else:
            pygame.draw.rect(screen, (0, 255, 0), (185, 107, 50, 20))
            pygame.display.update()
        if 185 < mouse[0] < 185 + 50 and 157 < mouse[1] < 157 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (185, 157, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(int(min_cutoff)), 1, (0, 0, 0))
            screen.blit(text,(240, 150))
            pygame.display.update()

            min_cutoff = min(min_cutoff + 10.0, 1000.0)
            text = font.render(str(int(min_cutoff)), 1, (0, 255, 0))
            screen.blit(text,(240, 150))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (185, 157, 50, 20))
            pygame.display.update()
        if 290 < mouse[0] < 290 + 50 and 157 < mouse[1] < 157 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (290, 157, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(int(min_cutoff)), 1, (0, 0, 0))
            screen.blit(text,(240, 150))
            pygame.display.update()

            min_cutoff = max(min_cutoff - 10.0, 500.0)
            text = font.render(str(int(min_cutoff)), 1, (0, 255, 0))
            screen.blit(text,(240, 150))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (290, 157, 50, 20))
            pygame.display.update()
        if 195 < mouse[0] < 195 + 50 and 207 < mouse[1] < 207 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (195, 207, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(int(max_cutoff)), 1, (0, 0, 0))
            screen.blit(text,(250, 200))
            pygame.display.update()

            max_cutoff = min(max_cutoff + 100.0, 9000.0)
            text = font.render(str(int(max_cutoff)), 1, (0, 255, 0))
            screen.blit(text,(250, 200))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (195, 207, 50, 20))
            pygame.display.update()
        if 300 < mouse[0] < 300 + 50 and 207 < mouse[1] < 207 + 20 and (pygame.mouse.get_pressed())[0]:
            pygame.draw.rect(screen, (255, 0, 0), (300, 207, 50, 20))
            font = pygame.font.Font("FreeSansBold.ttf", 20)
            text = font.render(str(int(max_cutoff)), 1, (0, 0, 0))
            screen.blit(text,(250, 200))
            pygame.display.update()

            max_cutoff = max(max_cutoff - 100.0, 5000.0)
            text = font.render(str(int(max_cutoff)), 1, (0, 255, 0))
            screen.blit(text,(250, 200))
            pygame.display.update()
        else:
            pygame.draw.rect(screen, (0, 255, 0), (300, 207, 50, 20))
            pygame.display.update()

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_x:

                    test = True

    print('***Done***')


def draw_main():

    screen.fill((0, 0, 0))
    pygame.display.update()

    text_title = "Real-time Sound Effect Box"
    text_q = "press \'Q\' for delay sound effect"
    text_w = "press \'W\' for flanging sound effect"
    text_e = "press \'E\' for robotization sound effect"
    text_r = "press \'R\' for wah-wah sound effect"
    text_end = "press \'X\' to end"

    font = pygame.font.Font("FreeSansBold.ttf", 40)
    text = font.render(text_title, 1, (0, 255, 0))
    screen.blit(text,(10, 10))
    pygame.display.update()
    font = pygame.font.Font("FreeSansBold.ttf", 20)
    text = font.render(text_q, 1, (0, 255, 0))
    screen.blit(text,(10, 80))
    pygame.display.update()
    text = font.render(text_w, 1, (0, 255, 0))
    screen.blit(text,(10, 110))
    pygame.display.update()
    text = font.render(text_e, 1, (0, 255, 0))
    screen.blit(text,(10, 140))
    pygame.display.update()
    text = font.render(text_r, 1, (0, 255, 0))
    screen.blit(text,(10, 170))
    pygame.display.update()
    text = font.render(text_end, 1, (0, 255, 0))
    screen.blit(text,(10, 300))
    pygame.display.update()

def draw_delay():

    screen.fill((0, 0, 0))
    pygame.display.update()

    text_title = "Delay Sound Effect Box"
    font = pygame.font.Font("FreeSansBold.ttf", 40)
    text = font.render(text_title, 1, (0, 255, 0))
    screen.blit(text,(10, 10))
    pygame.display.update()
    font = pygame.font.Font("FreeSansBold.ttf", 20)
    text = font.render("delay time:", 1, (0, 255, 0))
    screen.blit(text,(10, 100))
    pygame.display.update()
    text = font.render("feed forward gain:", 1, (0, 255, 0))
    screen.blit(text,(10, 150))
    pygame.display.update()
    text = font.render("feed back gain:", 1, (0, 255, 0))
    screen.blit(text,(10, 200))
    pygame.display.update()
    text = font.render("press \'X\' return to main page", 1, (0, 255, 0))
    screen.blit(text,(10, 300))
    pygame.display.update()
    text = font.render("left button to increase, right button to decrease", 1, (0, 255, 0))
    screen.blit(text, (10, 250))
    pygame.display.update()
    pygame.draw.rect(screen, (0, 255, 0), (125, 107, 50, 20))
    pygame.draw.rect(screen, (0, 255, 0), (195, 157, 50, 20))
    pygame.draw.rect(screen, (0, 255, 0), (165, 207, 50, 20))
    pygame.display.update()
    text = font.render("0.1", 1, (0, 255, 0))
    screen.blit(text,(180, 100))
    pygame.display.update()
    text = font.render("0.0", 1, (0, 255, 0))
    screen.blit(text,(250, 150))
    pygame.display.update()
    text = font.render("0.0", 1, (0, 255, 0))
    screen.blit(text,(220, 200))
    pygame.display.update()
    pygame.draw.rect(screen, (0, 255, 0), (215, 107, 50, 20))
    pygame.draw.rect(screen, (0, 255, 0), (285, 157, 50, 20))
    pygame.draw.rect(screen, (0, 255, 0), (255, 207, 50, 20))
    pygame.display.update()

def draw_flanging():

    screen.fill((0, 0, 0))
    pygame.display.update()

    text_title = "Flanging Sound Effect Box"
    font = pygame.font.Font("FreeSansBold.ttf", 40)
    text = font.render(text_title, 1, (0, 255, 0))
    screen.blit(text,(10, 10))
    pygame.display.update()
    font = pygame.font.Font("FreeSansBold.ttf", 20)
    text = font.render("speed:", 1, (0, 255, 0))
    screen.blit(text,(10, 100))
    pygame.display.update()
    text = font.render("depth:", 1, (0, 255, 0))
    screen.blit(text,(10, 150))
    pygame.display.update()
    text = font.render("press \'X\' return to main page", 1, (0, 255, 0))
    screen.blit(text,(10, 300))
    text = font.render("left button to increase, right button to decrease", 1, (0, 255, 0))
    screen.blit(text, (10, 250))
    pygame.display.update()
    pygame.display.update()
    pygame.draw.rect(screen, (0, 255, 0), (80, 107, 50, 20))
    pygame.draw.rect(screen, (0, 255, 0), (80, 157, 50, 20))
    pygame.display.update()
    text = font.render("1.00", 1, (0, 255, 0))
    screen.blit(text, (135, 100))
    pygame.display.update()
    text = font.render("0.1", 1, (0, 255, 0))
    screen.blit(text, (135, 150))
    pygame.display.update()
    pygame.draw.rect(screen, (0, 255, 0), (180, 107, 50, 20))
    pygame.draw.rect(screen, (0, 255, 0), (170, 157, 50, 20))
    pygame.display.update()

def draw_robotization():

    screen.fill((0, 0, 0))
    pygame.display.update()

    text_title = "Robotization Sound Effect Box"
    font = pygame.font.Font("FreeSansBold.ttf", 40)
    text = font.render(text_title, 1, (0, 255, 0))
    screen.blit(text,(10, 10))
    pygame.display.update()
    font = pygame.font.Font("FreeSansBold.ttf", 20)
    text = font.render("press \'X\' return to main page", 1, (0, 255, 0))
    screen.blit(text,(10, 300))
    pygame.display.update()

def draw_wahwah():

    screen.fill((0, 0, 0))
    pygame.display.update()

    text_title = "Wahwah Sound Effect Box"
    font = pygame.font.Font("FreeSansBold.ttf", 40)
    text = font.render(text_title, 1, (0, 255, 0))
    screen.blit(text,(10, 10))
    pygame.display.update()
    font = pygame.font.Font("FreeSansBold.ttf", 20)
    text = font.render("speed:", 1, (0, 255, 0))
    screen.blit(text,(10, 100))
    pygame.display.update()
    text = font.render("lowest frequency:", 1, (0, 255, 0))
    screen.blit(text,(10, 150))
    pygame.display.update()
    text = font.render("highest frequency:", 1, (0, 255, 0))
    screen.blit(text,(10, 200))
    pygame.display.update()
    text = font.render("press \'X\' return to main page", 1, (0, 255, 0))
    screen.blit(text,(10, 300))
    pygame.display.update()
    text = font.render("left button to increase, right button to decrease", 1, (0, 255, 0))
    screen.blit(text, (10, 250))
    pygame.display.update()
    pygame.draw.rect(screen, (0, 255, 0), (80, 107, 50, 20))
    pygame.draw.rect(screen, (0, 255, 0), (185, 157, 50, 20))
    pygame.draw.rect(screen, (0, 255, 0), (195, 207, 50, 20))
    pygame.display.update()
    text = font.render("500", 1, (0, 255, 0))
    screen.blit(text, (135, 100))
    pygame.display.update()
    text = font.render("100", 1, (0, 255, 0))
    screen.blit(text, (240, 150))
    pygame.display.update()
    text = font.render("5000", 1, (0, 255, 0))
    screen.blit(text, (250, 200))
    pygame.display.update()
    pygame.draw.rect(screen, (0, 255, 0), (185, 107, 50, 20))
    pygame.draw.rect(screen, (0, 255, 0), (290, 157, 50, 20))
    pygame.draw.rect(screen, (0, 255, 0), (300, 207, 50, 20))
    pygame.display.update()


def main_gui():

    draw_main()

    test = False

    while not test:
        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_x:
                    test = True
                if event.key == pygame.K_q:
                    draw_delay()
                    delay()
                    draw_main()
                if event.key == pygame.K_w:
                    draw_flanging()
                    flanging()
                    draw_main()
                if event.key == pygame.K_e:
                    draw_robotization()
                    robotization()
                    draw_main()
                if event.key == pygame.K_r:
                    draw_wahwah()
                    wahwah()
                    draw_main()

if __name__ == "__main__":

    main_gui()
