% ========================================================================
% Author: Matthew Pierce (V00126204)
% File: wah_filter.m
%
% Description: Phase Vocoder function for adding a wah-wah effect to
%              an audio signal
% Parameters: x - Input audio signal (must be a single-row vector)
%             fs - Sampling rate of x (Hz)
%             fc - Center "wah" frequency (Hz)
%             fb - Filter frequency bandwidth (Hz)
%             win - Window size
%             ra_hop - Analysis hop size
%             plot_type - Determines which frequency component to plot
%                         (Magnitude == 1, Phase == 0)
% ========================================================================
function y = wah_filter(x, fs, fc, fb, win, ra_hop, plot_type)
    
    % Zero pad the input signal for hopsize
    numWindows = ceil(length(x)/(ra_hop));
    numSamples = numWindows*(ra_hop);
    x = [x zeros(1, (numSamples - length(x)))];

    % Create the raised cosine function
    rcos_win = 1/2*(1-cos(2*pi*(1:win)/win));
    
    % Create matrices to store fft windows (mag and phase) over time
    fft_mag_matrix = zeros(numWindows, win);
    fft_phase_matrix = zeros(numWindows, win);
    
    % Set depth and speed values for the "wah" excursion and frequency
    depth = 0.45;
    speed = 0.8;
    
    % Initialize output signal
    y = zeros(1, length(x));
    
    for n=1:ra_hop:length(x)-win

        % Partition the signal into size win
        x_i = x(n:n+win-1);
        
        % Piecewise multiply the input segment with the cosine function
        x_rcos = x_i .* rcos_win;
        
        % Perform a circular shift on the time domain segment
        x_rcos_shft = circshift(x_rcos, [0, win/2]);
    
        % Take the fft of the windowed input
        X_rcos_shft = fft(x_rcos_shft);
 
        % Create an oscillator for the center frequency
        osc = fc*(1 + depth*cos(2*pi*speed*n/fs));
        
        % Create the two filter coefficient values (d uses the osc value)
        c = ( tan(pi*fb/fs) - 1 ) / ( tan(2*pi*fb/fs) + 1 );
        d = -cos(2*pi*osc/fs);
        
        % Get a complex exponential z value
        w = 2*pi*(0:win-1)/win;
        z = exp(w*j);
        
        % Set the second order allpass filter transfer function
        A_z = (-c + d*(1-c)*(z.^(-1)) + (z.^(-2))) ./ (1 + d*(1-c)*(z.^(-1)) - c*(z.^(-2)));
        
        % Set the second order bandpass filter transfer function using A_z
        H_z = (1/2) * (1 - A_z);

        % Filter this frequency domain segment
        Y_rcos_shft = X_rcos_shft .* H_z;
        
        % Store the magnitude and phase of this fft window in matrices
        mag = abs(Y_rcos_shft);
        fft_mag_matrix(floor(n/ra_hop)+1, 1:win) = mag;
        
        phase = angle(Y_rcos_shft);
        fft_phase_matrix(floor(n/ra_hop)+1, 1:win) = phase;
        
        % Find the time domain output (still shifted) using ifft
        y_rcos_shft = ifft(Y_rcos_shft);
    
        % "Unshift" the segment
        y_rcos = circshift(y_rcos_shft, [0, -win/2]);
        
        % Multiply by the cosine window again for output smoothing
        y_i = y_rcos .* rcos_win;
        
        % Get the length value
        len = length(y_i);

        % Overlap add the output
        y(n:n+len-1) = y(n:n+len-1) + y_i;
    
    end
	
    % Normalize output
    y = y./max(y);
    
	% Get the matrix dimensions
	[m, n] = size(fft_mag_matrix);
    
	if(plot_type==1)
		% Plot the 3D magnitude spectrum
		subplot(1, 1, 1);
		length(y)
		surf(linspace(0, fs, n), linspace(0, length(y)/fs, m), fft_mag_matrix);
		shading interp;
		axis([0 fs 0 length(y)/fs]);
		title('3D Magnitude Spectrum of Y');
		xlabel('Frequency (Hz)'), ylabel('Time (Seconds)'), zlabel('Magnitude');
    elseif (plot_type==0)
		% Plot the 3D phase spectrum
		subplot(1, 1, 1);
		unwrap_phase_matrix = unwrap(fft_phase_matrix); % Unwrap the phase
		surf(linspace(0, fs, n), linspace(0, length(y)/fs, m), unwrap_phase_matrix);
		shading interp;
		axis([0 fs 0 length(y)/fs]);
		title('3D Phase Spectrum of Y');
		xlabel('Frequency (Radians)'), ylabel('Time (Seconds)'), zlabel('Phase');
	end
end